package leclubdejudo;

import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.convChaineVersDate;


public class ClubJudo {
    
   public static List<Judoka> listeDesJudokas;  
   
   public  static  String[] lesCategories = { "super-légers","mi-légers","légers",
                                              "mi-moyens","moyens",
                                              "mi-lourd","lourds"
                                            };
   
   public static String  determineCategorie(String sexe, int poids){
     
       return lesCategories[determineIndiceCategorie(sexe,poids)];       
   }  
   
   public static int     determineIndiceCategorie(String sexe, int poids){

      return (  sexe.equals("M") )? chercheIndice( poids, limitesHommes ) : chercheIndice( poids, limitesFemmes );
   }  
   
   public static  void   afficherCategoriesPourSexe(String pSexe){
         
       System.out.printf("%-15s Jusqu'à      %3d  kg\n", lesCategories[0], limite(pSexe, 0));
        
       for (int i=1; i< lesCategories.length-1;i++){
                            
           System.out.printf("%-15s de  %d  kg à %3d  kg\n",lesCategories[i], limite(pSexe, i-1), limite(pSexe, i));     
       }
      
       int    indiceMax = lesCategories.length-1;
       System.out.printf("%-15s A partir de  %3d  kg\n", lesCategories[indiceMax],limite(pSexe,indiceMax-1)  
       );
   }
   
    
   public static int     ageJudoka(Judoka pMembre){ return ageEnAnnees(pMembre.dateNaiss);}
   
   
   public static String  categorieJudoka( Judoka pMembre ){ return determineCategorie(pMembre.sexe, pMembre.poids); }
   
   
   public static int  indiceCategorieJudoka( Judoka pMembre){ return determineIndiceCategorie(pMembre.sexe,pMembre.poids);}
   
   //<editor-fold defaultstate="collapsed" desc="CODE CREANT ET REMPLISSANT LA LISTE ListeDesMembres ">
  
  static {
      
      listeDesJudokas= new LinkedList<Judoka>();  
      
      Judoka p1= new Judoka();
      p1.nom="Durant";
      p1.prenom="Pierre";
      p1.sexe="M";
      p1.poids=83;
      p1.dateNaiss=convChaineVersDate("12/05/1993");
      p1.ville="Arras";
      p1.nbVictoires=8;
      
      Judoka p2= new Judoka();
      p2.nom="Martin";
      p2.prenom="Pierre";
      p2.sexe="M";
      p2.poids=75;
      p2.dateNaiss=convChaineVersDate("25/11/1991");
      p2.ville="Lens";
      p2.nbVictoires=6;
      
      Judoka p3= new Judoka();
      p3.nom="Lecoutre";
      p3.prenom="Thierry";
      p3.sexe="M";
      p3.poids=72;
      p3.dateNaiss=convChaineVersDate("05/08/1992");
      p3.ville="Arras";
      p3.nbVictoires=5;
      
      Judoka p4= new Judoka();
      p4.nom="Duchemin";
      p4.prenom="Fabienne";
      p4.sexe="F";
      p4.poids=61;
      p4.dateNaiss=convChaineVersDate("14/3/1992");
      p4.ville="Lens";
      p4.nbVictoires=10;
      
      Judoka p5= new Judoka();
      p5.nom="Duchateau";
      p5.prenom="Jacques";
      p5.sexe="M";
      p5.poids=91;
      p5.dateNaiss=convChaineVersDate("18/07/1992");
      p5.ville="Bapaume";
      p5.nbVictoires=4;
      
      Judoka p6= new Judoka();
      p6.nom="Lemortier";
      p6.prenom="Laurent";
      p6.sexe="M";
      p6.poids=76;
      p6.dateNaiss=convChaineVersDate("18/02/1989");
      p6.ville="Arras";
      p6.nbVictoires=12;
      
      Judoka p7= new Judoka();
      p7.nom="Dessailles";
      p7.prenom="Sabine";
      p7.sexe="F";
      p7.poids=68;
      p7.dateNaiss=convChaineVersDate("23/03/1990");
      p7.ville="Arras";
      p7.nbVictoires=3;
      
      Judoka p8= new Judoka();
      p8.nom="Bataille";
      p8.prenom="Boris";
      p8.sexe="M";
      p8.poids=102;
      p8.dateNaiss=convChaineVersDate("17/11/1991");
      p8.ville="Vitry-En-Artois";
      p8.nbVictoires=7;
      
      Judoka p9= new Judoka();
      p9.nom="Lerouge";
      p9.prenom="Laëtitia";
      p9.sexe="F";
      p9.poids=46;
      p9.dateNaiss=convChaineVersDate("09/10/1992");
      p9.ville="Lens";
      p9.nbVictoires=4;
      
      
      Judoka p10= new Judoka();
      p10.nom="Renard";
      p10.prenom="Paul";
      p10.sexe="M";
      p10.poids=68;
      p10.dateNaiss=convChaineVersDate("16/08/1992");
      p10.ville="Lens";
      p10.nbVictoires=9;
      
      Judoka p11= new Judoka();
      p11.nom="Durant";
      p11.prenom="Jacques";
      p11.sexe="M";
      p11.poids=75;
      p11.dateNaiss=convChaineVersDate("13/04/1990");
      p11.ville="Arras";
      p11.nbVictoires=4;
      
      Judoka p12= new Judoka();
      p12.nom="Delespaul";
      p12.prenom="Martine";
      p12.sexe="F";
      p12.poids=55;
      p12.dateNaiss=convChaineVersDate("25/02/1991");
      p12.ville="Lens";
      p12.nbVictoires=6;
      
      
      ajouterLesJudokasALaListe(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12);
      
  }
  
  static void ajouterLesJudokasALaListe(Judoka p1, Judoka p2, Judoka p3, Judoka p4, Judoka p5, Judoka p6, Judoka p7, Judoka p8, Judoka p9, Judoka p10, Judoka p11, Judoka p12) {
      
      listeDesJudokas.add(p1); listeDesJudokas.add(p2);  listeDesJudokas.add(p3); 
      listeDesJudokas.add(p4); listeDesJudokas.add( p5); listeDesJudokas.add( p6);
      listeDesJudokas.add(p7); listeDesJudokas.add(p8);  listeDesJudokas.add(p9); 
      listeDesJudokas.add(p10); listeDesJudokas.add(p11);listeDesJudokas.add(p12);
  }
  //</editor-fold>
   
   //<editor-fold defaultstate="collapsed" desc="RESTE DU CODE">  
   
   private static int    chercheIndice(int poids, int[] tableauLimites ){
   
      int i;
      
      for(i=0;i<tableauLimites.length;i++)
      {         
         if( poids<tableauLimites[i] )
         {  
            break;
         }
      }
   
      return i;
   }  
   
  
    private static int limite(String pSexe, int indice) {
        return (pSexe.equals("M")) ? limitesHommes[indice]-1:limitesFemmes[indice]-1;
    }
   
   static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
   static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
   

  

  
  
     //</editor-fold>

}








