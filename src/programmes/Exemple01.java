package programmes;

import static leclubdejudo.ClubJudo.listeDesJudokas;
import leclubdejudo.Judoka;
import static utilitaires.UtilDate.convDateVersChaine;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static leclubdejudo.Tris.trierParNomPrenom;

public class Exemple01 {

  public void executer() {
        
    afficherTitre();    
    trierLaListeDesJudokas();
    
    for ( Judoka judoka : listeDesJudokas ) { traiterJudoka( judoka ); } 
    
    System.out.println();   
  }
 
  void afficherTitre() { System.out.printf("\n Liste des judokas du Club le %-8s\n\n", aujourdhuiChaine()); }
  
  void traiterJudoka( Judoka pJudoka ) {
     
        System.out.printf( " %-10s %-10s %-2s %-10s %4d kg %-20s %2d Victoires\n",
                
          pJudoka.nom, pJudoka.prenom,
          pJudoka.sexe,
          convDateVersChaine(pJudoka.dateNaiss),
          pJudoka.poids,
          pJudoka.ville,
          pJudoka.nbVictoires
        );
   }

  void trierLaListeDesJudokas() { trierParNomPrenom( listeDesJudokas );}
}


