
package programmes;

import static leclubdejudo.ClubJudo.listeDesJudokas;
import static leclubdejudo.ClubJudo.ageJudoka;
import leclubdejudo.Judoka;
import static leclubdejudo.Tris.trierParNomPrenom;
import static utilitaires.UtilDate.convDateVersChaine;

public class Question01 {
    
   public  void executer() {
         
      afficherTitre();
       
      trierParNomPrenom(listeDesJudokas);
       
      for(Judoka membre : listeDesJudokas){
            
            traiterMembre( membre);        
      } 
        
      System.out.println();
    }

    void traiterMembre( Judoka pMembre) {
            
        String format="  %-10s %-10s %-2s %-10s %2d ans %3dKg\n";    
        System.out.printf(format, 
                          pMembre.nom, pMembre.prenom, pMembre.sexe,convDateVersChaine( pMembre.dateNaiss),
                          ageJudoka(pMembre),
                          pMembre.poids
        );
    }

    void afficherTitre() {
        
        System.out.printf("\n Liste des judokas du Club au %-10s:\n\n",utilitaires.UtilDate.aujourdhuiChaine());
    }

}






