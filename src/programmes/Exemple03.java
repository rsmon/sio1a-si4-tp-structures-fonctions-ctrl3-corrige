package programmes;

import static leclubdejudo.ClubJudo.determineCategorie;
import static leclubdejudo.ClubJudo.afficherCategoriesPourSexe;

public class Exemple03 {

   
    public void executer() {
        
        System.out.println("Catégories Hommes\n");    
        afficherCategoriesPourSexe("M");
        System.out.println(); 
            
        
        System.out.print("Catégorie pour un homme de 81 kg: ");   
        System.out.println(determineCategorie("M", 81));
        System.out.println();
        
        System.out.println("Catégories Femmes\n");    
        afficherCategoriesPourSexe("F");
        System.out.println(); 
        
        
        System.out.print("Catégorie pour une femme de 60 kg: ");   
        System.out.println(determineCategorie("F", 60));
        System.out.println();        
    }
}



