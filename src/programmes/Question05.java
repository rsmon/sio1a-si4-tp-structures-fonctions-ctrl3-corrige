
package programmes;

import static leclubdejudo.ClubJudo.listeDesJudokas;
import leclubdejudo.Judoka;
import static leclubdejudo.ClubJudo.ageJudoka;

public class Question05 {

    public  void executer() {
          
       int   totalAge=0;
       float moyenneAge;
       
       for( Judoka membre : listeDesJudokas ){
           
            totalAge+=ageJudoka(membre);
       } 
        
       moyenneAge=(float)totalAge/listeDesJudokas.size();
       
       System.out.printf("\n Moyenne d'age des judokas: %2.2f ans au %-10s\n\n", moyenneAge, utilitaires.UtilDate.aujourdhuiChaine());
    }
}

