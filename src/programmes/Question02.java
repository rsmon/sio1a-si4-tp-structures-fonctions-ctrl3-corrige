package programmes;

import static leclubdejudo.ClubJudo.listeDesJudokas;
import leclubdejudo.Judoka;
import static leclubdejudo.Tris.trierParNbVictoiresDecroissant;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Question02 {

    public  void executer() {
         
       afficherTitre();
           
       trierLaListeDesMembres();
       
       for ( Judoka membre : listeDesJudokas ){
           
            traiterMembre(membre);   
       } 
        
       System.out.println();       
    }
  
    void afficherTitre() {
     
        System.out.printf("\n Liste des judokas cumulant plus de 6 victoires au %-10s\n\n", aujourdhuiChaine());
    }
    
    void traiterMembre( Judoka pJudoka ) {
    
        if( pJudoka.nbVictoires>6 ) {
          
            String format=" %-10s %-10s %3d victoires\n";
            System.out.printf( format, pJudoka.nom, pJudoka.prenom, pJudoka.nbVictoires );          
     
        }
    }

    void trierLaListeDesMembres() {
        
        trierParNbVictoiresDecroissant( listeDesJudokas );
    }
}

