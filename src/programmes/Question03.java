
package programmes;

import static utilitaires.UtilDate.aujourdhuiChaine;
import leclubdejudo.Judoka;
import static leclubdejudo.ClubJudo.listeDesJudokas;
import static leclubdejudo.Tris.trierParNomPrenom;

public class Question03 {

    public  void executer() {
         
       afficherTitre();
          
       trierlaListeDesMembres();
       
       for(Judoka membre : listeDesJudokas){
           
           traiterMembre(membre);   
       } 
        
       System.out.println("\n");
              
    }

    void afficherTitre() {
        
        System.out.printf("\n Liste des membres féminins habitant Lens au %10s\n\n",aujourdhuiChaine());
    }

    void trierlaListeDesMembres() {
        
        trierParNomPrenom(listeDesJudokas);
    }

    void traiterMembre(Judoka pMembre) {
        
        if( pMembre.sexe.equals("F") && pMembre.ville.equals("Lens")) {
          
          System.out.printf("   %-10s %-10s %3d kg %-10s\n", pMembre.nom, pMembre.prenom, pMembre.poids, pMembre.ville);        
     
        }
    }
}

