
package programmes;

import static leclubdejudo.ClubJudo.listeDesJudokas;
import leclubdejudo.Judoka;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static leclubdejudo.ClubJudo.categorieJudoka;

public class Question04 {

    public void executer() {
         
       afficherTitre();
        
        trierLaListeDesMembres();
       
       for( Judoka membre : listeDesJudokas ) {
           
           traiterMembre(membre); 
       }  
       
        System.out.println();
    }

    void traiterMembre(Judoka pMembre) {
        
        if ( categorieJudoka(pMembre).equals("mi-moyens")) {
            
             System.out.printf(" %-10s %-10s %3dKg %-15s\n",pMembre.nom, pMembre.prenom, pMembre.poids, pMembre.ville);
        }
        
    }

    void afficherTitre() {
        
        System.out.printf("\n Liste des judokas appartenant à la catégorie de poids mi-moyens le %-8s:\n\n",aujourdhuiChaine());
    }

    void trierLaListeDesMembres() {
        
        leclubdejudo.Tris.trierParSexeNomPrenom(listeDesJudokas);
    }

}

